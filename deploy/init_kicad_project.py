import json
from pathlib import Path

cwd = Path.cwd()
pro_file = [*cwd.glob("*.kicad_pro")][0]

with open(pro_file, mode="r") as f:
    project_contents = json.load(f)

field_names = [
    {"name": "Manufacturer", "url": False, "visible": False},
    {"name": "Manufacturer Ref", "url": False, "visible": False},
    {"name": "Vendor", "url": False, "visible": False},
    {"name": "Vendor Ref", "url": False, "visible": False},
    {"name": "Vendor URL", "url": True, "visible": False},
]

project_contents["schematic"]["drawing"]["field_names"] = field_names

project_contents["libraries"]["pinned_symbol_libs"] = [
    "TCR-Components",
    "TCR-Connectors",
    "TCR-Converters",
    "TCR-Switches",
]
project_contents["libraries"]["pinned_footprints_libs"] = [
    "TCR-Logo",
    "TCR-Components",
    "TCR-Connectors",
    "TCR-Converters",
    "TCR-Passives",
    "TCR-Switches",
]

with open(pro_file, mode="w") as f:
    json.dump(project_contents, f, indent=2)
