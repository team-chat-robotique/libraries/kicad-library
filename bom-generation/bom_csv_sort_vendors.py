#
# Example python script to generate a BOM from a KiCad generic netlist
#
# Example: Sorted and Grouped CSV BOM
#

"""
    @package
    Output: CSV (comma-separated)
    Grouped By: Value, Footprint
    Sorted By: Vendor
    Fields: Ref, Qnty, Value, Cmp name, Footprint, Description, Vendor

    Command line:
    python "pathToFile/bom_csv_sort_vendors.py" "%I" "%O.csv"
"""

import csv
import sys
from pathlib import Path
from typing import TextIO

# Import the KiCad python helper module and the csv formatter
import kicad_netlist_reader
import kicad_utils


def fromNetlistText(aText: str):
    """A helper function to filter/convert a string read in netlist"""
    if ":" in aText and aText.split(":")[0].startswith("TCR-"):
        return ":".join(aText.split(":")[1:])
    return aText


in_file = Path(sys.argv[1])
out_file = Path(sys.argv[2])

# netlist instance
net = kicad_netlist_reader.netlist(in_file)
kiprjmod = Path(net.getSource()).parent

out_dir = kiprjmod / "boms"
out_dir.mkdir(parents=True, exist_ok=True)
outstem = out_file.stem

# Get all of the components in groups of matching parts + values
# (see ky_generic_netlist_reader.py)
grouped = net.groupComponents()


def create_outfile(path: str) -> TextIO:
    """Open a file to write to,
    if the file cannot be opened,
    output to stdout instead

    Args:
        path (str): output path
    """
    try:
        f = kicad_utils.open_file_writeUTF8(path, "w")
    except IOError:
        e = f"Can't open output file for writing: {path}"
        print(__file__, ":", e, sys.stderr)
        f = sys.stdout
    return f


# Vendor methods
def is_wurth(comp: kicad_netlist_reader.comp) -> bool:
    """Würth components
    MF == Würth Elektronik
    """
    WURTH = "Würth Elektronik"
    return comp.getField("MF") == WURTH


def is_rs(comp: kicad_netlist_reader.comp) -> bool:
    """RS Online components
    RS Number != ''
    """
    return len(comp.getField("RS Number")) > 0


# Sort groups by vendor type, based on vendor filters
vendors = {"wurth-elektronik": is_wurth, "rs-online": is_rs}
groups = {"remaining": []}
for group in grouped:
    component = group[-1]
    for vendor, func in vendors.items():
        if func(component):
            groups.setdefault(vendor, []).append(group)
            break
    else:
        groups["remaining"].append(group)

# Unified output
unified_fname = f"bom-{outstem}-unified.csv"
f = create_outfile(out_dir / unified_fname)
# Create a new csv writer object to use as the output formatter
out_unified = csv.writer(
    f, lineterminator="\n", delimiter=",", quotechar='"', quoting=csv.QUOTE_ALL
)
out_unified.writerow(
    [
        "Ref",
        "Value",
        "Qnty",
        "Vendor",
        "Vendor Number",
        "Manufacturer",
        "Name",
        "Footprint",
        "Datasheet",
    ]
)


# Export funcs
def export_wurth(out: TextIO, groups: list):
    """Output all of the components information"""
    # Header
    out.writerow(
        [
            "Marque",
            "Modèle",
            "Type",
            "Site de commande",
            "Qté",
            "Utilisation",
            "Footprint",
            "URL",
        ]
    )

    for group in groups:
        # Add the reference of every component in the group and keep a reference
        # to the component so that the other data can be filled in once per group
        refdes = [fromNetlistText(comp.getRef()) for comp in group]
        refs = ", ".join(refdes)
        c: kicad_netlist_reader.comp = group[-1]

        # Fill in the component groups common data
        out.writerow(
            [
                fromNetlistText(c.getField("MF")),
                fromNetlistText(c.getField("Product name")),
                fromNetlistText(c.getPartName()),
                fromNetlistText(c.getField("MF")),
                len(group),
                refs,
                fromNetlistText(c.getFootprint()),
                fromNetlistText(c.getDatasheet()),
            ]
        )
        out_unified.writerow(
            [
                refs,
                fromNetlistText(c.getValue()),
                len(group),
                "wurth-elektronik",
                fromNetlistText(c.getPartName()),
                fromNetlistText(c.getField("MF")),
                fromNetlistText(c.getField("Product name")),
                fromNetlistText(c.getFootprint()),
                fromNetlistText(c.getDatasheet()),
            ]
        )


def export_rs(out: TextIO, groups: list):
    """Output all of the components information"""
    # Header
    out.writerow(
        [
            "Marque",
            "Modèle",
            "RS Number",
            "Type",
            "Site de commande",
            "Qté",
            "Utilisation",
            "Footprint",
            "URL",
        ]
    )

    """Output all of the components information"""
    for group in groups:
        # Add the reference of every component in the group and keep a reference
        # to the component so that the other data can be filled in once per group
        refdes = [fromNetlistText(comp.getRef()) for comp in group]
        refs = ", ".join(refdes)
        c: kicad_netlist_reader.comp = group[-1]

        # Fill in the component groups common data
        out.writerow(
            [
                fromNetlistText(c.getField("MF")),
                fromNetlistText(c.getField("Product name")),
                fromNetlistText(c.getField("RS Number")),
                fromNetlistText(c.getPartName()),
                fromNetlistText(c.getField("MF")),
                len(group),
                refs,
                fromNetlistText(c.getFootprint()),
                fromNetlistText(c.getDatasheet()),
            ]
        )
        out_unified.writerow(
            [
                refs,
                fromNetlistText(c.getValue()),
                len(group),
                "rs-online",
                fromNetlistText(c.getField("RS Number")),
                fromNetlistText(c.getField("MF")),
                fromNetlistText(c.getField("Product name")),
                fromNetlistText(c.getFootprint()),
                fromNetlistText(c.getDatasheet()),
            ]
        )


def export_generic(out: TextIO, groups: list):
    # Output a set of rows for a header providing general information
    out.writerow(
        [
            "Ref",
            "Value",
            "Qnty",
            "Manufacturer",
            "Name",
            "Footprint",
            "Datasheet",
        ]
    )

    """Output all of the components information"""
    for group in groups:
        # Add the reference of every component in the group and keep a reference
        # to the component so that the other data can be filled in once per group
        refdes = [fromNetlistText(comp.getRef()) for comp in group]
        refs = ", ".join(refdes)
        c: kicad_netlist_reader.comp = group[-1]

        # Fill in the component groups common data
        out.writerow(
            [
                refs,
                fromNetlistText(c.getValue()),
                len(group),
                fromNetlistText(c.getField("MF")),
                fromNetlistText(c.getPartName()),
                fromNetlistText(c.getFootprint()),
                fromNetlistText(c.getDatasheet()),
            ]
        )
        out_unified.writerow(
            [
                refs,
                fromNetlistText(c.getPartName()),
                len(group),
                "generic",
                fromNetlistText(c.getField("RS Number")),
                fromNetlistText(c.getField("MF")),
                fromNetlistText(c.getField("Product name")),
                fromNetlistText(c.getFootprint()),
                fromNetlistText(c.getDatasheet()),
            ]
        )


for vendor, vend_groups in groups.items():
    outfname = f"bom-{outstem}-{vendor}.csv"
    f = create_outfile(out_dir / outfname)
    # Create a new csv writer object to use as the output formatter
    out = csv.writer(
        f, lineterminator="\n", delimiter=",", quotechar='"', quoting=csv.QUOTE_ALL
    )
    export_funcs = {
        "wurth-elektronik": export_wurth,
        "rs-online": export_rs,
        "remaining": export_generic,
    }
    export_funcs[vendor](out, vend_groups)
