#
# Example python script to generate a BOM from a KiCad generic netlist
#
# Example: Sorted and Grouped CSV BOM
#

"""
    @package
    Output: CSV (comma-separated)
    Grouped By: Value, Footprint
    Sorted By: Ref
    Fields: Ref, Value, Qnty, Manuf., Cmp name, Footprint, Datasheet

    Command line:
    python "pathToFile/bom_csv_grouped_teamchat.py" "%I" "%O.csv"
"""

import csv
import sys

# Import the KiCad python helper module and the csv formatter
import kicad_netlist_reader
import kicad_utils


# A helper function to filter/convert a string read in netlist
# currently: do nothing
def fromNetlistText(aText):
    if ":" in aText and aText.split(":")[0].startswith("TCR-"):
        return ":".join(aText.split(":")[1:])
    return aText


# Generate an instance of a generic netlist, and load the netlist tree from
# the command line option. If the file doesn't exist, execution will stop
net = kicad_netlist_reader.netlist(sys.argv[1])

# Open a file to write to, if the file cannot be opened output to stdout
# instead
try:
    f = kicad_utils.open_file_writeUTF8(sys.argv[2], "w")
except IOError:
    e = "Can't open output file for writing: " + sys.argv[2]
    print(__file__, ":", e, sys.stderr)
    f = sys.stdout

# Create a new csv writer object to use as the output formatter
out = csv.writer(
    f, lineterminator="\n", delimiter=",", quotechar='"', quoting=csv.QUOTE_ALL
)

# Output a set of rows for a header providing general information
out.writerow(["Source:", net.getSource()])
out.writerow(["Date:", net.getDate()])
out.writerow(["Tool:", net.getTool()])
out.writerow(["Generator:", sys.argv[0]])
out.writerow(["Component Count:", len(net.components)])
out.writerow(
    [
        "Ref",
        "Value",
        "Qnty",
        "Manufacturer",
        "Name",
        "Footprint",
        "Datasheet",
    ]
)


# Get all of the components in groups of matching parts + values
# (see ky_generic_netlist_reader.py)
grouped = net.groupComponents()

# Output all of the component information
for group in grouped:
    # Add the reference of every component in the group and keep a reference
    # to the component so that the other data can be filled in once per group
    refdes = [fromNetlistText(comp.getRef()) for comp in group]
    refs = ", ".join(refdes)
    c: kicad_netlist_reader.comp = group[-1]

    # Fill in the component groups common data
    out.writerow(
        [
            refs,
            fromNetlistText(c.getValue()),
            len(group),
            fromNetlistText(c.getField("MF")),
            fromNetlistText(c.getPartName()),
            fromNetlistText(c.getFootprint()),
            fromNetlistText(c.getDatasheet()),
        ]
    )
