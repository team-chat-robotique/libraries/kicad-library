.PHONY: update podman update-symbols update-footprints

update: update-symbols update-footprints

# docker:
# 	docker run -v .:/repo -w /repo --rm -it \
# 		kicad/kicad:8.0 bash -c "sudo apt-get update && sudo apt-get install -y build-essential && bash"

podman:
	podman run -v .:/repo -w /repo --rm -it \
		docker.io/kicad/kicad:8.0 bash -c "sudo apt-get update && sudo apt-get install -y build-essential && bash"

update-symbols:
	for f in lib-kicad/*.kicad_sym; do \
		echo "[SYM] Upgrading $$f"; \
		kicad-cli sym upgrade $$f; \
	done

update-footprints:
	for f in lib-kicad/*.pretty; do \
		echo "[FP] Upgrading $$f"; \
		kicad-cli fp upgrade $$f; \
	done
